var data = [];
var index;
var id;
var name;
var resp;

//adding the data to the grid
addItem = function() {
    var id = document.getElementById("id").value;
    var name = document.getElementById("name").value;
    var resp = document.getElementById('response').value;
    var temp = {id, name, resp};
    data.push(temp);
    displayGrid();
    document.getElementById("id").value = "";
    document.getElementById("name").value = "";
    document.getElementById('response').value = "";
}

//displaying the data in the grid
displayGrid = function() {
  var receiveData = "";
  for(var i=0; i<data.length; i++){
      receiveData += "<tr>";
      receiveData += "<td>"+ data[i].id +"</td>";
      receiveData += "<td>"+ data[i].name + "</td>";
      receiveData += "<td>"+ data[i].resp + "</td>";
      receiveData += "<td><button class='btn btn-primary' onClick='buttonClick("+i+","+data[i].id+")' data-toggle='modal' data-target='#editModal'>Edit</button></td>";
      receiveData += "<td><button class='btn btn-danger' onClick='buttonClick("+i+")' data-toggle='modal' data-target='#deleteModal'>Delete</button></td>";
      receiveData += "</tr>";
  }
  document.getElementById('tid').innerHTML = receiveData;
}

buttonClick = function(ind, eid) {
  index = ind;
  id = eid;
  document.getElementById('eid').value = data[index].id;
  document.getElementById('ename').value = data[index].name;
  document.getElementById('eresponse').value = data[index].resp;
}
//deleting the data from the grid
deleteData = function() {
  data.splice(index, 1);
  displayGrid();
}

function editItem() {
  data[index].id = document.getElementById('eid').value;
  data[index].name = document.getElementById('ename').value;
  data[index].resp = document.getElementById('eresponse').value;
  displayGrid();
}
